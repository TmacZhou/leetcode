package L0003;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class LongestString {
    public static int lengthOfLongestSubstring(String s) {
        int maxLength = 0;
        int rk = -1;
        Set<Character> strings = new HashSet<>();
        int n = s.length();
        for(int i = 0; i < n; i++){
            if(i != 0){
                System.out.println(strings.toString());
                strings.remove(s.charAt(i-1));
            }
            while (rk + 1 < n && !strings.contains(s.charAt(rk + 1))){
                strings.add(s.charAt(rk + 1));
                ++ rk;
            }
            maxLength = Math.max(maxLength, rk - i + 1);
        }
        return maxLength;
    }

    public static int lengthOfLongestSubstring3(String s) {
        int n = s.length();
        if (n==0) return 0;
        HashMap<Character, Integer> map = new HashMap<>();
        int maxLength = 0;
        int left = 0;
        for(int i = 0; i < n; i++){
            if(map.containsKey(s.charAt(i))){
                left = Math.max(left, map.get(s.charAt(i)) + 1);
            }
            map.put(s.charAt(i), i);
            maxLength = Math.max(maxLength, i - left + 1);
        }
        return maxLength;
    }

    public static int lengthOfLongestSubstring2(String s) {
        // 哈希集合，记录每个字符是否出现过
        Set<Character> occ = new HashSet<Character>();
        int n = s.length();
        // 右指针，初始值为 -1，相当于我们在字符串的左边界的左侧，还没有开始移动
        int rk = -1, ans = 0;
        for (int i = 0; i < n; ++i) {
            if (i != 0) {
                // 左指针向右移动一格，移除一个字符
                occ.remove(s.charAt(i - 1));
            }
            while (rk + 1 < n && !occ.contains(s.charAt(rk + 1))) {
                // 不断地移动右指针
                occ.add(s.charAt(rk + 1));
                ++rk;
            }
            // 第 i 到 rk 个字符是一个极长的无重复字符子串
            ans = Math.max(ans, rk - i + 1);
        }
        return ans;
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring3("abba"));
    }
}
