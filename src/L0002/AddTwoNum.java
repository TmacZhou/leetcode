package L0002;

import java.math.BigDecimal;

public class AddTwoNum {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        BigDecimal b1 = listNodeToInt(l1);
        BigDecimal b2 = listNodeToInt(l2);
        BigDecimal sum = b1.add(b2);
        return intToListNode(sum);
    }

    public ListNode addTwoNumbers2(ListNode l1, ListNode l2) {
        int offset = 0;
        ListNode head = null, tail = null;
        while(l1 != null || l2 != null){
            int firstNum = l1 == null ? 0 : l1.val;
            int secondNum = l2 == null ? 0 : l2.val;
            int sum = firstNum + secondNum + offset;
            if(head == null){
                head = tail = new ListNode(sum % 10);
            }else{
                tail.next = new ListNode(sum % 10);
                tail = tail.next;
            }
            offset = sum / 10;
            if(l1 !=null){
                l1 = l1.next;
            }
            if(l2 !=null){
                l2 = l2.next;
            }
        }
        if(offset > 0){
            tail.next = new ListNode(1);
        }
        return head;
    }

    private BigDecimal listNodeToInt(ListNode listNode){
        StringBuilder sb = new StringBuilder();
        while (listNode != null ){
            sb.append(listNode.val);
            listNode = listNode.next;
        }
        return new BigDecimal(sb.reverse().toString());
    }

    private ListNode intToListNode(BigDecimal num){
        String s = String.valueOf(num);
        ListNode head = new ListNode(Integer.parseInt(String.valueOf(s.charAt(s.length() - 1))));
        for(int i = s.length() - 2; i >= 0; i--){
            ListNode temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = new ListNode(Integer.parseInt(String.valueOf(s.charAt(i))));
        }
        return head;
    }

    public void addNode(ListNode head, int num){
        ListNode temp = head;
        while(temp.next != null){
            temp = temp.next;
        }
        temp.next = new ListNode(num);
    }

    public static void main(String[] args) {
        AddTwoNum addTwoNum = new AddTwoNum();
        ListNode first = new ListNode(9);


        ListNode second = new ListNode(1);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        addTwoNum.addNode(second, 9);
        ListNode listNode = addTwoNum.addTwoNumbers2(first, second);
    }
}
